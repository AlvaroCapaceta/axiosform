const btnBuscar = document.getElementById("btnBuscar");
const btnLimpiar = document.getElementById("btnLimpiar");

function cargarAjax() {
  const url = "https://jsonplaceholder.typicode.com/users";

  axios
    .get(url)
    .then((res) => {
      buscar(res.data);
    })
    .catch((reject) => {
      console.log("Surgio un error" + reject);
    });
}

function buscar(data) {
  const id = document.getElementById("txtId").value;
  const txtNombre = document.getElementById("txtNombre");
  const txtUserName = document.getElementById("txtUserName");
  const txtEmail = document.getElementById("txtEmail");
  const txtCalle = document.getElementById("txtCalle");
  const txtNumero = document.getElementById("txtNumero");
  const txtCiudad = document.getElementById("txtCiudad");

  document.querySelector("form").reset();

  for (let item of data) {
    if (item.id == id) {
      txtNombre.value = item.name;
      txtUserName.value = item.username;
      txtEmail.value = item.email;
      txtCalle.value = item.address.street;
      txtNumero.value = item.address.suite;
      txtCiudad.value = item.address.city;
      return;
    }
  }

  if (txtNombre.value == "") {
    alert("No se encontraron datos");
  }
}

const limpiar = () => {
  document.querySelector("form").reset();
};

btnBuscar.addEventListener("click", (event) => {
  event.preventDefault();
  cargarAjax();
});

btnLimpiar.addEventListener("click", limpiar);

document.addEventListener("DOMContentLoaded", () => {
  const preventDefault = (string) => {
    document.querySelectorAll(string).forEach((node) =>
      node.addEventListener("keypress", (e) => {
        if (e.keyCode == 13) {
          e.preventDefault();
        }
      })
    );
  };

  preventDefault("input[type=text]");
  preventDefault("input[type=number]");
});
